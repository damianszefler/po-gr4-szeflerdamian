package pl.edu.uwm.wmii.damianszefler.laboratorium01;
import java.util.Scanner;

class Zadanie2_2_5 {

    public static <string> void main(String[] args) {

        int n;
        int wynik=0;
        int spr1;
        int spr2 = -1;

        System.out.println("podaj dlugosc ciagu: ");
        Scanner liczbacyfr = new Scanner(System.in);
        n = liczbacyfr.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartość ciagu " + i + ":");
            Scanner liczba1 = new Scanner(System.in);
            spr1 = liczba1.nextInt();

            if(spr1>0 && spr2>0){
                wynik+=1;
            }
            spr2=spr1;
        }
        System.out.println("Takich par jest " + wynik);
    }
}

