package pl.edu.uwm.wmii.damianszefler.laboratorium01;

import java.util.Scanner;

class Zadanie2_b { public static <string> void main(String[] args) {

    int n;
    String m;
    String a1;
    String wynik = "Szukany ciag Liczb : ";
    System.out.println("podaj dlugosc ciagu: ");
    Scanner liczbacyfr = new Scanner(System.in);
    n = liczbacyfr.nextInt();
    System.out.println(n);
    for (int i = 1; i <= n; i++) {
        System.out.println("Podaj liczbe " + i + ":");
        Scanner liczba = new Scanner(System.in);
        m = liczba.nextLine();
        if(Integer.parseInt(m)%3==0 && Integer.parseInt(m)%5!=0){
            wynik += m;
            wynik += ", ";
        }
    }
    System.out.println(wynik);
}
}

