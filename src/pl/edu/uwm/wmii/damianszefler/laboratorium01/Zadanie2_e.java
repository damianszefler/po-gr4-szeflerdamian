package pl.edu.uwm.wmii.damianszefler.laboratorium01;
import java.lang.Math;
import java.util.Scanner;

class Zadanie2_e {

    private static int silnia(int n) {
        if (n < 2)
            return 1;
        return n * silnia(n - 1);
    }
    public static <string> void main(String[] args) {

        int n;
        int spr;
        String wynik = "Szukany ciag to: ";
        System.out.println("podaj dlugosc ciagu: ");
        Scanner liczbacyfr = new Scanner(System.in);
        n = liczbacyfr.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartość ciagu " + i + ":");
            Scanner liczba3 = new Scanner(System.in);
            spr = liczba3.nextInt();
            System.out.println(Math.pow(2, i)+ ":::"+ silnia(i));

            if (Math.pow(2, i) < spr && spr < silnia(i)) {

                wynik += spr;
                wynik += ", ";
            }

        }
        System.out.println(wynik);
    }
}

