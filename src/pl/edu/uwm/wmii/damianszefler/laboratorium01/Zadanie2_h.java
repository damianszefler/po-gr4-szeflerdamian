package pl.edu.uwm.wmii.damianszefler.laboratorium01;
import java.lang.Math;
import java.util.Scanner;

class Zadanie2_h {

    public static <string> void main(String[] args) {

        int n;
        int spr;
        String wynik = "Szukany ciag to: ";
        System.out.println("podaj dlugosc ciagu: ");
        Scanner liczbacyfr = new Scanner(System.in);
        n = liczbacyfr.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.print("podaj wartość ciagu " + i + ":");
            Scanner liczba3 = new Scanner(System.in);
            spr = liczba3.nextInt();

            if (Math.abs(spr)<Math.pow(2,spr)) {

                wynik += spr;
                wynik += ", ";
            }

        }
        System.out.println(wynik);
    }
}

