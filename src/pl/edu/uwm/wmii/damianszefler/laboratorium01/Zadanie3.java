package pl.edu.uwm.wmii.damianszefler.laboratorium01;
import java.util.Scanner;
import java.lang.Math;

class Zadanie3 { public static void main(String[] args) {
    float a;
    float wynik=0;
    int n;
    System.out.println("Podaj liczbe ile cyfr chcesz dodac:");
    Scanner liczbacyfr = new Scanner(System.in);
    n = liczbacyfr.nextInt();
        for(int i=1; i<=n;i++) {
            System.out.println("Podaj liczbe " + i + ".");
            Scanner odczyt = new Scanner(System.in);
            a = odczyt.nextFloat();
            wynik += Math.abs(a);
        }
    System.out.println(wynik);
}
}