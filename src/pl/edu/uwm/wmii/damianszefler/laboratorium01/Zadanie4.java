package pl.edu.uwm.wmii.damianszefler.laboratorium01;
import java.lang.Math;
import java.util.Scanner;

class Zadanie4 { public static void main(String[] args) {
    float a;
    float wynik=0;
    int n;
    System.out.println("Podaj liczbe ile pierwiastków chcesz dodac:");
    Scanner liczbacyfr = new Scanner(System.in);
    n = liczbacyfr.nextInt();
    for(int i=1; i<=n;i++) {
        System.out.println("Podaj liczbe " + i + ".");
        Scanner odczyt = new Scanner(System.in);
        a = odczyt.nextFloat();
        wynik += Math.sqrt(Math.abs(a));
    }
    System.out.println(wynik);
}
}