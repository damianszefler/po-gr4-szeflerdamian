package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;

class Zadanie1_a {
        public static void main(String[] args) {
            Random rand = new Random();
            int n;
            System.out.println("Podaj liczbe ile cyfr chcesz dodac:");
            Scanner liczbacyfr = new Scanner(System.in);
            n = liczbacyfr.nextInt();
            int[] tablica = new int[n];
            int parzysta=0;
            int nieparzysta=0;
            for (int i = 0; i < n; i++)
                tablica[i] = rand.nextInt(2000) - 1000;

            for (int i = 0; i < n; i++){
                System.out.println(tablica[i]);
                if(tablica[i]%2==0)
                    parzysta++;
                if(tablica[i]%2!=0)
                    nieparzysta++;
            }

            System.out.println("Liczb parzystych jest: " + parzysta + ", a nieparzystych: " + nieparzysta);
    }
}
