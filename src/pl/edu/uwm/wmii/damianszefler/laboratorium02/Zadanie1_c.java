package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;

class Zadanie1_c {
    public static void main(String[] args) {
        Random rand = new Random();
        int n;
        System.out.println("Podaj ilość ele tablicy:");
        Scanner liczbacyfr = new Scanner(System.in);
        n = liczbacyfr.nextInt();
        int[] tablica = new int[n];
        int ile = 0;
        for (int i = 0; i < n; i++)
            tablica[i] = rand.nextInt(2000) - 1000;
        int max = tablica[0];
        for (int i = 0; i < n; i++){
            System.out.println(tablica[i]);
            if(tablica[i]>max) {
                max=tablica[i];
            }
            else if(tablica[i]==max)
                ile++;
        }

        System.out.println("Najwiekszy element tablicy to " + max+ ", wystepuje: " + ile +" razy");
    }
}