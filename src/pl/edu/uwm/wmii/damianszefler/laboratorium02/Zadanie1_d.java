package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;

class Zadanie1_d {
    public static void main(String[] args) {
        Random rand = new Random();
        int n;
        System.out.println("Podaj ilość ele tablicy:");
        Scanner liczbacyfr = new Scanner(System.in);
        n = liczbacyfr.nextInt();
        int[] tablica = new int[n];
        int ujemna=0;
        int dodatnia=0;
        for (int i = 0; i < n; i++)
            tablica[i] = rand.nextInt(2000) - 1000;

        for (int i = 0; i < n; i++){
            System.out.println(tablica[i]);
            if(tablica[i]>0)
                dodatnia+=tablica[i];
            if(tablica[i]<0)
                ujemna+=tablica[i];
        }

        System.out.println("Suma dodatnich : " + dodatnia + " ,a ujemnuch: " + ujemna);
    }
}