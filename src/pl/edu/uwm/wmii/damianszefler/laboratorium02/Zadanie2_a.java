package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;

class Zadanie2_a {

    private static void generuj(int[] tab, int n, int min, int max) {

        Random r = new Random();
            for (int j = 0; j < n; ++j) {
                tab[j] = r.nextInt(max) + min;
        }
    }
    private static int ileParzystych(int[] tablica) {
        int parzysta = 0;

        for (int aTablica : tablica) {
            if (aTablica % 2 == 0)
                parzysta++;
        }
        return parzysta;
    }
    private static int ileNieparzystych(int[] tablica) {
        int nieparzysta = 0;

        for (int aTablica : tablica) {
            if (aTablica % 2 != 0)
                nieparzysta++;
        }
        return nieparzysta;
    }
    public static void main(String[] args) {
        Random rand = new Random();

        System.out.println("Podaj ilosc ele tablicy:");
        Scanner liczbacyfr1 = new Scanner(System.in);
        int n = liczbacyfr1.nextInt();
        System.out.println("Podaj poczatek przedziału:");
        Scanner liczbacyfr2 = new Scanner(System.in);
        int min = liczbacyfr2.nextInt();
        System.out.println("Podaj koniec przedziału:");
        Scanner liczbacyfr3 = new Scanner(System.in);
        int max = liczbacyfr3.nextInt();

        int[] tab = new int[n];
        generuj(tab,n,min,max);
        for (int i = 0; i < n; i++)
            System.out.println(tab[i]);
        System.out.println("Parzystych jest "+ileParzystych(tab)+" ,nieparzystych jest "+ileNieparzystych(tab));
}
}
