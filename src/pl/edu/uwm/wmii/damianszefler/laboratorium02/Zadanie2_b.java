package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;

class Zadanie2_b {

    private static void generuj(int[] tab, int n, int min, int max) {

        Random r = new Random();
        max+=2;
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(max) + min;
        }
    }
    private static int ileDodatnich(int[] tablica) {
       int dodatnia=0;
        for (int aTablica : tablica) {
            if (aTablica > 0)
                dodatnia++;
        }
        return dodatnia;
    }
    private static int ileUjemnych(int[] tablica) {
        int ujemna=0;
        for (int aTablica : tablica) {
            if (aTablica < 0)
                ujemna++;
        }
        return ujemna;
    }
    private static int ileZer(int[] tablica) {
        int zero=0;
        for (int aTablica : tablica) {
            if (aTablica == 0)
                zero++;
        }
        return zero;
    }

    public static void main(String[] args) {
        Random rand = new Random();

        System.out.println("Podaj ilosc ele tablicy:");
        Scanner liczbacyfr1 = new Scanner(System.in);
        int n = liczbacyfr1.nextInt();
        System.out.println("Podaj poczatek przedziału:");
        Scanner liczbacyfr2 = new Scanner(System.in);
        int min = liczbacyfr2.nextInt();
        System.out.println("Podaj koniec przedziału:");
        Scanner liczbacyfr3 = new Scanner(System.in);
        int max = liczbacyfr3.nextInt();

        int[] tab = new int[n];
        generuj(tab,n,min,max);
        for (int i = 0; i < n; i++)
            System.out.println(tab[i]);
        System.out.println("Dodatnich jest "+ileDodatnich(tab)+" ,ujemnych jest "+ileUjemnych(tab)+" , a zer jest "+ileZer(tab));
    }
}
