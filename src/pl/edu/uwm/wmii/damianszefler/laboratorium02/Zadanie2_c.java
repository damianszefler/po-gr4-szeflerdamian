package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;

class Zadanie2_c {

    private static void generuj(int[] tab, int n, int min, int max) {

        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(max) + min;
        }
    }
    private static int ileMax(int[] tablica) {
        int max=tablica[0];
        int ile=0;
        for (int aTablica : tablica) {
            System.out.println(aTablica);
            if (aTablica > max) {
                max = aTablica;
            } else if (aTablica == max)
                ile++;
        }
        System.out.println("Najwiekszy element tablicy to " + max);
        return ile;
    }

    public static void main(String[] args) {
        Random rand = new Random();

        System.out.println("Podaj ilosc ele tablicy:");
        Scanner liczbacyfr1 = new Scanner(System.in);
        int n = liczbacyfr1.nextInt();
        System.out.println("Podaj poczatek przedziału:");
        Scanner liczbacyfr2 = new Scanner(System.in);
        int min = liczbacyfr2.nextInt();
        System.out.println("Podaj koniec przedziału:");
        Scanner liczbacyfr3 = new Scanner(System.in);
        int max = liczbacyfr3.nextInt();

        int[] tab = new int[n];
        generuj(tab,n,min,max);
        for (int i = 0; i < n; i++)
            System.out.println(tab[i]);
        System.out.println("Powtórzeń najwiekszej liczby to " + ileMax(tab));
    }
}
