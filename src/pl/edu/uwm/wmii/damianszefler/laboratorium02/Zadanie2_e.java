package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;
import java.lang.Math;
class Zadanie2_e {

    private static void generuj(int[] tab, int n, int min, int max) {

        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            if(min>=0)
                tab[j] = r.nextInt(max) + min;
            if(min<0)
                tab[j] = r.nextInt(max+Math.abs(min)) + min;
        }
    }
    private static int ileDodCiag(int[] tablica) {
        int dodciag=0;
        int ile=1;
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i - 1] > 0 && tablica[i] > 0){
                ile++;}
            else{
                dodciag=ile;
                ile = 1;}
        }
        return dodciag;
    }


    public static void main(String[] args) {
        Random rand = new Random();

        System.out.println("Podaj ilosc ele tablicy:");
        Scanner liczbacyfr1 = new Scanner(System.in);
        int n = liczbacyfr1.nextInt();
        System.out.println("Podaj poczatek przedziału:");
        Scanner liczbacyfr2 = new Scanner(System.in);
        int min = liczbacyfr2.nextInt();
        System.out.println("Podaj koniec przedziału:");
        Scanner liczbacyfr3 = new Scanner(System.in);
        int max = liczbacyfr3.nextInt();

        int[] tab = new int[n];
        generuj(tab,n,min,max);
        for (int i = 0; i < n; i++)
            System.out.println(tab[i]);
        System.out.println("Najdluzszy ciag dodatnich to: " + ileDodCiag(tab));
    }
}
