package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;
import java.lang.Math;
class Zadanie2_g {

    private static void generuj(int[] tab, int n, int min, int max) {

        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            if(min>=0)
                tab[j] = r.nextInt(max) + min;
            if(min<0)
                tab[j] = r.nextInt(max+Math.abs(min)) + min;
        }
    }
    private static void odwrtFrag(int[] tablica) {
        System.out.println("Podaj poczatek przedziału do odwrocenia:");
        Scanner liczbacyfr2 = new Scanner(System.in);
        int prawy = liczbacyfr2.nextInt();
        System.out.println("Podaj koniec przedziału do odwrocenia:");
        Scanner liczbacyfr3 = new Scanner(System.in);
        int lewy = liczbacyfr3.nextInt();

        System.out.println("____________________________________________________________");
        for (int i = lewy+1; i > prawy+1; i--)
            System.out.println(tablica[i]);
    }


    public static void main(String[] args) {
        Random rand = new Random();

        System.out.println("Podaj ilosc ele tablicy:");
        Scanner liczbacyfr1 = new Scanner(System.in);
        int n = liczbacyfr1.nextInt();
        System.out.println("Podaj poczatek przedziału:");
        Scanner liczbacyfr2 = new Scanner(System.in);
        int min = liczbacyfr2.nextInt();
        System.out.println("Podaj koniec przedziału:");
        Scanner liczbacyfr3 = new Scanner(System.in);
        int max = liczbacyfr3.nextInt();

        int[] tab = new int[n];
        generuj(tab,n,min,max);

        for (int i = 0; i < n; i++)
            System.out.println(tab[i]);

        odwrtFrag(tab);
    }
}
