package pl.edu.uwm.wmii.damianszefler.laboratorium02;
import java.util.Scanner;
import java.util.Random;

class Zadanie3 {


    private static void ilorazmacierz(int k, int l, int m) {
        Scanner in = new Scanner(System.in);
        Random generator = new Random();

        int[][] tab1 = new int[k][l];
        int[][] tab2 = new int[l][m];
        int[][] tab3 = new int[k][m];

        for (int i = 0; i < k; i++) {
            for (int j = 0; j < l; j++)
                tab1[i][j] = generator.nextInt(200) - 100;
        }

        for (int i = 0; i < l; i++) {
            for (int j = 0; j < m; j++)
                tab2[i][j] = generator.nextInt(200) - 100;
        }

        for (int i = 0; i < k; i++) {
            for (int j = 0; j < l; j++)
                System.out.print(tab1[i][j] + " ");
            System.out.print("\n");
        }

        System.out.print("\n  \n");
        for (int i = 0; i < l; i++)
            for (int j = 0; j < m; j++) {
                System.out.print(tab2[i][j] + " ");
            System.out.print("\n");
        }
        System.out.print("___________________________________________________________________________________");

        for(int i = 0; i < m ; i++)
        {
            for(int j = 0 ; j < k ; j++ )
            {
                int suma= 0;
                for(int o = 0; o < l ; o++)
                {
                    int liczba;
                    liczba = tab1[j][o] * tab2[o][i];
                    suma += liczba;
                }
                tab3[j][i] = suma;
            }
        }
        System.out.print("\n  \n");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                System.out.print(tab3[j][i] + " ");
            }
            System.out.print("\n");
        }
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczne k: [1:10] ");
        Scanner liczbacyfr1 = new Scanner(System.in);
        int k = liczbacyfr1.nextInt();
        System.out.println("Podaj liczne n: [1:10] ");
        Scanner liczbacyfr2 = new Scanner(System.in);
        int n = liczbacyfr2.nextInt();
        System.out.println("Podaj liczne m: [1:10] ");
        Scanner liczbacyfr3 = new Scanner(System.in);
        int m= liczbacyfr3.nextInt();
        ilorazmacierz(k, n, m);
    }
}