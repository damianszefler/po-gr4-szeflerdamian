package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.util.Scanner;
import java.lang.String;
class Zadanie1_a {

    private static int countChar(String str, char c) {
    int temp=0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i)==c) {
                temp++;
            }
        }
        return temp;
    }

    public static void main(String[] args) {
        System.out.println("Podaj literke:");
        Scanner litera = new Scanner(System.in);
        int n = litera.next().charAt(0);

        System.out.println(countChar("Damian", 'a'));
    }
}
