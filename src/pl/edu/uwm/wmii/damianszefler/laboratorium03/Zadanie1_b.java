package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.util.Scanner;
import java.lang.String;
class Zadanie1_b {

    private static int countSubStr(String str, String subStr) {
        int temp1=0;
        String temp2="";
        for(int i=0; i<str.length(); i++){
            temp2+=str.charAt(i);
            System.out.println(temp2);
            if(temp2.toLowerCase().contains(subStr.toLowerCase())) {
                temp1++;
                temp2="";
            }
        }
        return temp1;
    }

    public static void main(String[] args) {
        System.out.println("Podaj subStr:");
        Scanner litera = new Scanner(System.in);
        String n = litera.nextLine();

        System.out.println(countSubStr("Damiandamiandamian", n));
    }
}
