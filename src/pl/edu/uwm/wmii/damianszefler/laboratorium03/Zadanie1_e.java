package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.lang.String;
class Zadanie1_e {

    private static int[] where(String str, String subStr) {

        int[] temp = new int[str.length()];
        int i=0;
        int lastIndex = 0;
        while(lastIndex != -1){

            lastIndex = str.indexOf(subStr,lastIndex);

            if(lastIndex != -1){
                temp[i]=lastIndex;
                i++;
                lastIndex += subStr.length();
            }
        }
        return temp;
    }


    public static void main(String[] args) {

        int tab[];
        tab=where("damiandamiandamian", "damian");
        for(int i=0; i<tab.length; i++)
            System.out.println(tab[i]);
    }
}
