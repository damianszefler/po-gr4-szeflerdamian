package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.lang.String;
class Zadanie1_f {

    private static String change(String str) {
        StringBuffer temp = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLowerCase(str.charAt(i))) {
                temp.append(str.toUpperCase().charAt(i));
            } else {
                temp.append(str.toLowerCase().charAt(i));
            }
        }
        return temp.toString();
    }

    public static void main(String[] args) {
        System.out.println(change("witam SERdecznie1"));
    }
}