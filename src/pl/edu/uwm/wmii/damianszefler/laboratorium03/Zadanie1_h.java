package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.lang.String;
class Zadanie1_h {

    private static String nice(String str, int n, char c) {
        StringBuffer temp = new StringBuffer();
        for(int i=str.length()-1; i>=0;i--){
            if(i%n==0 && i!=0) {
                temp.append(str.charAt(i));
                temp.append(c);
            }
            else{
                temp.append(str.charAt(i));
            }
        }
        return temp.toString();
    }

    public static void main(String[] args) {
        System.out.println(nice("1123345789346890569029378",2,'-'));
    }
}
