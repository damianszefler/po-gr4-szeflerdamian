package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.lang.String;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

class Zadanie2 {
    private static int countChar(String str, char c) {
        int temp=0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i)==c) {
                temp++;
            }
        }
        return temp;
    }

    private static String readFile(String path) throws IOException {
         String File="";
        String path1 = new java.io.File(".").getCanonicalPath();
        FileReader fileReader = new FileReader(path1+"\\src\\pl\\edu\\uwm\\wmii\\damianszefler\\laboratorium03\\"+path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String textLine = bufferedReader.readLine();

        do {
            File+=textLine+"\n";
            textLine = bufferedReader.readLine();
        } while(textLine != null);
        bufferedReader.close();
        System.out.print(File);
        return File;
    }
    public static void main(String[] args) throws IOException {

        System.out.println(countChar(readFile("text"), 'e'));

    }
}

