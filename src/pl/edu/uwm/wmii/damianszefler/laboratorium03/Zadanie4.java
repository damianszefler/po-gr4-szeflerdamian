package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.math.BigInteger;
class Zadanie4 {
    private static BigInteger countMak(int n) {
        BigInteger temp1= new  BigInteger("1");
        BigInteger temp2= new  BigInteger("1");
        if(n==1)
            return temp1;
        else{
            temp1=temp1.add(temp1);
            temp2=temp2.add(temp1);
        for(int i=0; i<n*n-2; i++)
        {
            temp1=temp1.multiply(new BigInteger("2"));
            temp2=temp2.add(temp1);
        }
        return temp2;
        }
    }

    public static void main(String[] args) {

        System.out.println(countMak (4));

    }
}