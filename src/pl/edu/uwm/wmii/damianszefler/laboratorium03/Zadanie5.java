package pl.edu.uwm.wmii.damianszefler.laboratorium03;
import java.math.BigDecimal;
import java.math.RoundingMode;
class Zadanie5 {
    private static BigDecimal lokata(double k, double s, int l){

        BigDecimal kapital =  new BigDecimal(k);
        BigDecimal stopa =  new BigDecimal(s);

        for(int i =0 ; i < l ; i++)
        {
            kapital = kapital.add(kapital.multiply(stopa));

        }
        return kapital.setScale(2, RoundingMode.HALF_UP);
    }

    public static void main(String[] args) {

        System.out.println(lokata (1000, 0.05, 5));

    }
}