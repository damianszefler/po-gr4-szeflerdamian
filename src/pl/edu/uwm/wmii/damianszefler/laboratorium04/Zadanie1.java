package pl.edu.uwm.wmii.damianszefler.laboratorium04;

import java.util.ArrayList;

class Zadanie1 {

    private static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        a.addAll(b);
        return a;
    }


    public static void main(String[] args) {

        ArrayList<Integer> arrlist = new ArrayList<>(5);

        arrlist.add(12);
        arrlist.add(20);
        arrlist.add(45);

        System.out.println("Printing list1:");
        for (Integer number : arrlist) {
            System.out.println("Number = " + number);
        }
        ArrayList<Integer> arrlist2 = new ArrayList<>(5);

        arrlist2.add(25);
        arrlist2.add(30);
        arrlist2.add(31);
        arrlist2.add(35);

        System.out.println("Printing list2:");
            for (Integer number : arrlist2) {
                System.out.println("Number = " + number);
            }
        System.out.println("Printing list1+list2:");
        for (Integer number : append(arrlist, arrlist2)){
            System.out.println("Number = " + number);
        }

    }

}
