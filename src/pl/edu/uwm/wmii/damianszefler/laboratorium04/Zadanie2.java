package pl.edu.uwm.wmii.damianszefler.laboratorium04;

import java.util.ArrayList;

class Zadanie2 {

    private static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> tmp1 = b;
        ArrayList<Integer> tmp2 = a;
        int j=0;
        if(a.size()<=b.size()){
             tmp1 = a;
             tmp2 = b;
        }
        for(int i=0; i<tmp1.size(); i+=2){
            tmp1.add(i,tmp2.get(j));
            j++;
        }
        if (j > 0) {
            tmp2.subList(0, j).clear();
        }

        tmp1.addAll(tmp2);

        return tmp1;
    }


    public static void main(String[] args) {

        ArrayList<Integer> arrlist = new ArrayList<>(5);

        arrlist.add(1);
        arrlist.add(2);
        arrlist.add(3);
        arrlist.add(4);
        arrlist.add(5);

        System.out.println("Printing list1:");
        for (Integer number : arrlist) {
            System.out.println("Number = " + number);
        }
        ArrayList<Integer> arrlist2 = new ArrayList<>(5);

        arrlist2.add(11);
        arrlist2.add(22);
        arrlist2.add(33);
        arrlist2.add(44);

        System.out.println("Printing list2:");
        for (Integer number : arrlist2) {
            System.out.println("Number = " + number);
        }
        System.out.println("Printing list1+list2:");
        for (Integer number : merge(arrlist, arrlist2)){
            System.out.println("Number = " + number);
        }

    }

}
