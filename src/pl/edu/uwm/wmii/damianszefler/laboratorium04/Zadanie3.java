package pl.edu.uwm.wmii.damianszefler.laboratorium04;

import java.util.ArrayList;

class Zadanie3 {

    private static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> tmp = new ArrayList<>(a.size() + b.size());
        int a1=0;
        int b1=0;
        for (int i=0; i<a.size()+b.size();i++){
            if (a1<a.size() && a.get(a1)<=b.get(b1)) {
                tmp.add(a.get(a1));
                a1++;
            }
            else if (b1<b.size())
            {
                tmp.add(b.get(b1));
                b1++;
            }
        }
        return tmp;
    }
    public static void main(String[] args) {

        ArrayList<Integer> arrlist = new ArrayList<>(5);

        arrlist.add(1);
        arrlist.add(2);
        arrlist.add(3);
        arrlist.add(4);
        arrlist.add(5);

        System.out.println("Printing list1:");
        for (Integer number : arrlist) {
            System.out.println("Number = " + number);
        }
        ArrayList<Integer> arrlist2 = new ArrayList<>(5);

        arrlist2.add(1);
        arrlist2.add(3);
        arrlist2.add(5);
        arrlist2.add(7);
        arrlist2.add(9);

        System.out.println("Printing list2:");
        for (Integer number : arrlist2) {
            System.out.println("Number = " + number);
        }
        System.out.println("Printing list1+list2:");
        for (Integer number : merge(arrlist, arrlist2)){
            System.out.println("Number = " + number);
        }

    }

}
