package pl.edu.uwm.wmii.damianszefler.laboratorium04;
import java.util.ArrayList;
class Zadanie4 {

    private static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> tmp = new ArrayList<>(a.size());
        for (int i = a.size()-1; i>=0; i--) {
            tmp.add(a.get(i));
        }
        return tmp;
    }


    public static void main(String[] args) {

        ArrayList<Integer> arrlist = new ArrayList<>(5);

        arrlist.add(12);
        arrlist.add(20);
        arrlist.add(45);

        System.out.println("Printing list1:");
        for (Integer number : arrlist) {
            System.out.println("Number = " + number);
        }

        System.out.println("Printing reversed list");
        for (Integer number : reversed(arrlist)) {
            System.out.println("Number = " + number);
        }
    }
}
