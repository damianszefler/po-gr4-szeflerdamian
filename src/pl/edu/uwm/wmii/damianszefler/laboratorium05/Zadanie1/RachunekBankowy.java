package pl.edu.uwm.wmii.damianszefler.laboratorium05.Zadanie1;

class RachunekBankowy {

    private static double rocznaStopaProcentowa;
    double saldo;

    public RachunekBankowy(double saldo){
        this.saldo=saldo;
    }

    public void obliczMiesieczneOdsetki() {
            saldo += (rocznaStopaProcentowa)/12;
    }

    public static void setRocznaStopaProcentowa (double n) {
        rocznaStopaProcentowa = n;
    }

}
