package pl.edu.uwm.wmii.damianszefler.laboratorium05.Zadanie2;

class IntegerSet {

    boolean[] tab = new boolean[100];

    public IntegerSet() {
        for (int i = 0; i < 100; i++) {
            this.tab[i] = false;
        }
    }

    public static boolean[] union(boolean[] tab1, boolean[] tab2) {
        boolean[] tmptab = new boolean[100];
        for (int i = 0; i < 100; i++) {
            if (tab1[i] || tab2[i]) {
                tmptab[i] = true;
            }
        }
        return tmptab;
    }
    public static boolean[] intersection(boolean[] tab1, boolean[] tab2) {
        boolean[] tmptab = new boolean[100];
        for (int i = 0; i < 100; i++) {
            if (tab1[i] && (tab2[i])) {
                tmptab[i] = true;
            }
        }
        return tmptab;
    }
    public void insertElement(int n) {
        if (!this.tab[n])
            this.tab[n] = true;
    }
    public void deleteElement(int n) {
        if (this.tab[n])
            this.tab[n] = false;
    }
    public String toString() {
       String tmp="";
        for (int i = 0; i < 100; i++) {
            if (this.tab[i])
                tmp += "1";
            else
                tmp += "0";
        }
        return tmp;
    }
    public void equals(boolean[] tab1){
        for (int i = 0; i < 100; i++)
        {
            if(this.tab[i]!=tab1[i]) {
                System.out.println("Zbiory NIE są sobie równe!");
                return;
            }
        }
        System.out.println("Zbiory są sobie równe!");
    }

}
