package pl.edu.uwm.wmii.damianszefler.laboratorium05.Zadanie2;


class IntegerSetTest {
    public static void main(String[] args) {
        IntegerSet liczba1 = new IntegerSet();
        IntegerSet liczba2 = new IntegerSet();
        IntegerSet liczba3 = new IntegerSet();
        liczba1.insertElement(0);
        liczba2.insertElement(1);
        liczba1.insertElement(2);
        liczba2.insertElement(2);

        liczba3.insertElement(0);
        liczba3.insertElement(2);

        liczba2.insertElement(3);
        System.out.println(liczba2.tab[3]);
        liczba2.deleteElement(3);
        System.out.println(liczba2.tab[3]);

        System.out.println(IntegerSet.union(liczba1.tab, liczba2.tab)[10]);
        System.out.println(IntegerSet.intersection(liczba1.tab, liczba2.tab)[1]);

        System.out.println(liczba2.tab[1]);

        System.out.println(liczba1.toString());

        liczba1.equals(liczba2.tab);
    }
}
