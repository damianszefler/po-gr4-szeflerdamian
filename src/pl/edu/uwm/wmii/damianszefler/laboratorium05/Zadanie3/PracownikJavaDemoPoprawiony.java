package pl.edu.uwm.wmii.damianszefler.laboratorium05.Zadanie3;

import java.time.LocalDate;

class PracownikJavaDemoPoprawiony
{
    public static void main(String[] args)
    {
        Pracownik[] personel = new Pracownik[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new Pracownik("Karol Cracker", 75000, LocalDate.of(2001, 12, 15));
        personel[1] = new Pracownik("Henryk Hacker", 50000, LocalDate.of(2003, 10, 1));
        personel[2] = new Pracownik("Antoni Tester", 40000, LocalDate.of(2005, 3, 15));

        // zwieksz pobory każdego pracownika o 20%
        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownik.getNextId(); // wywołanie metody statycznej
        System.out.println("Następny dostępny id = " + n);

    }
}

class Pracownik
{
    public Pracownik(String nazwisko, double pobory, LocalDate data)
    {
        this.nazwisko = nazwisko;
        this.pobory = pobory;
        this.dataZa = data;


        id = nextId;
        ++nextId;
    }

    public String nazwisko()
    {
        return nazwisko;
    }

    public double pobory()
    {
        return pobory;
    }

    public LocalDate dataZatrudnienia()
    {
        return dataZa;
    }

    public void zwiekszPobory(double procent)
    {
        double podwyzka = pobory * procent / 100;
        pobory += podwyzka;
    }

    public int id()
    {
        return id;
    }

    public void setId()
    {
        id = nextId;
        ++nextId;
    }

    public static int getNextId()
    {
        return nextId;
    }

    private String nazwisko;
    private double pobory;
    private LocalDate dataZa;

    private int id;
    private static int nextId = 1;
}