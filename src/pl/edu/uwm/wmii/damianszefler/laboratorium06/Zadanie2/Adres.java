package pl.edu.uwm.wmii.damianszefler.laboratorium06.Zadanie2;

class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieskania;
    private String miasto;
    private int kod_pocztowy;

    public Adres(String ulica, int numer_domu, int numer_mieskania, String miasto, int kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieskania=numer_mieskania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    public Adres(String ulica, int numer_domu, String miasto, int kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    public void pokaz()
    {
        System.out.println(this.kod_pocztowy+" "+this.miasto);
        System.out.println(this.ulica+" "+this.numer_domu+" "+this.numer_mieskania);
    }
    public boolean przed(Adres obiekt){
        return this.kod_pocztowy < obiekt.kod_pocztowy;
    }
}
