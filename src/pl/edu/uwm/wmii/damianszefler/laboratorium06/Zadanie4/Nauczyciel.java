package pl.edu.uwm.wmii.damianszefler.laboratorium06.Zadanie4;

public class Nauczyciel extends Osoba{
    private int pensja;
    Nauczyciel(String nazwisko, int rok_urodzenia, int pensja){
        super(nazwisko, rok_urodzenia);
        this.pensja=pensja;
    }
    public String toString(){
        return super.toString()+" Pensja:"+this.pensja;
    }

    public int getPensja() {
        return pensja;
    }
}

