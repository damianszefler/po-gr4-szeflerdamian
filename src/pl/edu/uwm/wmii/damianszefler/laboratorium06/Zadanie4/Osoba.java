package pl.edu.uwm.wmii.damianszefler.laboratorium06.Zadanie4;

class Osoba {
    private String nazwisko;
    private int rok_urodzenia;
    Osoba(String nazwisko, int rok_urodzenia){
        this.nazwisko=nazwisko;
        this.rok_urodzenia=rok_urodzenia;
    }
    public String toString(){
        return "Nazwisko: "+this.nazwisko+" Rok urodzenia: "+this.rok_urodzenia;
    }
    public String getNazwisko() {
        return nazwisko;
    }
    public int getRok_urodzenia() {
        return rok_urodzenia;
    }
}



