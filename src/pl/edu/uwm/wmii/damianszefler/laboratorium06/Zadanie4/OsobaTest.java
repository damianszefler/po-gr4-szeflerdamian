package pl.edu.uwm.wmii.damianszefler.laboratorium06.Zadanie4;


class OsobaTest {
    public static void main(String[] args) {
        Student student = new Student("Szefler", 1996, "Informatyka");
        Nauczyciel nauczyciel1 = new Nauczyciel("Prus", 1895, 3200);
        Nauczyciel nauczyciel2 = new Nauczyciel("Konieczko", 1975, 1200);
        System.out.println(student.toString());
        System.out.println(nauczyciel1.toString());
        System.out.println(nauczyciel2.toString());
        System.out.print(student.getKierunek());
    }
}
