package pl.edu.uwm.wmii.damianszefler.laboratorium06.Zadanie4;

public class Student extends Osoba {
    private String kierunek;
    public Student (String nazwisko, int rok_urodzenia, String kierunek){
        super(nazwisko, rok_urodzenia);
        this.kierunek=kierunek;
    }
    public String toString(){
        return super.toString()+" Kierunek:"+this.kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }
}