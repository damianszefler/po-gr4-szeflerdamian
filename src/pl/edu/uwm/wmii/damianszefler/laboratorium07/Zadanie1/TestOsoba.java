package pl.edu.uwm.wmii.damianszefler.laboratorium07.Zadanie1;
import java.time.LocalDate;

class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski", "Jan", LocalDate.of(1954, 5, 21), true,5000 ,LocalDate.of(1994, 5, 21));
        ludzie[1] = new Student("Nowak", "Małgorzata", LocalDate.of(1954, 5, 21),false, "Informatyka", 3.56);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko()+" "+p.getImiona()+" "+p.getDataUr()+" Płeć:"+p.getPlec() + ": " + p.getOpis());
        }
    }
}

abstract class Osoba
{
    Osoba(String nazwisko, String imiona, LocalDate data, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.data_ur= data;
        this.plec=plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
     public String getImiona(){
        return imiona;
    }
    public LocalDate getDataUr(){
        return data_ur;
    }
    public boolean getPlec(){
        return plec;
    }
    private String nazwisko;
    private String imiona;
    private LocalDate data_ur;
    private boolean plec;
}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String imiona, LocalDate data_ur, boolean plec, double pobory, LocalDate data_zatrudnienia)
    {
        super(nazwisko, imiona, data_ur, plec);
        this.pobory = pobory;
        this.data_zatrudnienia = data_zatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    private LocalDate getDataZat(){return data_zatrudnienia;}


    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł, zatrudniony:", pobory)+getDataZat();
    }

    private double pobory;
    private LocalDate data_zatrudnienia;
}


class Student extends Osoba
{
    public Student(String nazwisko, String imiona, LocalDate data_ur, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, data_ur, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek + " średnia: "+getSredniaOcen();
    }
    private double getSredniaOcen(){return sredniaOcen;}
    public double setSredniaOcen(double n){return sredniaOcen=n;}

    private String kierunek;
    private double sredniaOcen;
}

