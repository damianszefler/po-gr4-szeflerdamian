package pl.edu.uwm.wmii.damianszefler.laboratorium07.Zadanie3;

import pl.imiajd.szefler.Instrument;
import pl.imiajd.szefler.Flet;
import pl.imiajd.szefler.Fortepian;
import pl.imiajd.szefler.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;

class InstrumentTest {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>(5);
        orkiestra.add(new Flet("Skiba",LocalDate.of(2010, 12, 12)));
        orkiestra.add(new Skrzypce("Stradivarius",LocalDate.of(1643, 12, 12)));
        orkiestra.add(new Fortepian("Yamaha",LocalDate.of(1992, 12, 12)));
        orkiestra.add(new Flet("Skiba",LocalDate.of(2000, 12, 12)));
        orkiestra.add(new Flet("Skiba",LocalDate.of(2018, 12, 12)));


        for (Instrument anOrkiestra : orkiestra) {
            System.out.println(anOrkiestra.dzwiek());
        }
    }
}
