package pl.edu.uwm.wmii.damianszefler.laboratorium08.Zadanie1;
import pl.imiajd.szefler.*;

import java.time.LocalDate;
import java.util.Arrays;

class TestOsoba {

    public static void main(String[] args)
    {
        Osoba[] grupa = new Osoba[5];

        grupa[0] = new Osoba("Skiba", LocalDate.of(1996, 12, 12));
        grupa[1] = new Osoba("Tabasko", LocalDate.of(1997, 5, 7));
        grupa[2] = new Osoba("Skiba", LocalDate.of(1997, 9, 11));
        grupa[3] = new Osoba("Stachek", LocalDate.of(1997, 5, 7));
        grupa[4] = new Osoba("Szefler", LocalDate.of(1996, 1, 5));
        for(int i = 0; i<5; i++)
            System.out.println(grupa[i].toString());

        Arrays.sort(grupa, Osoba::compareTo);
        System.out.println("_____________________________________________________________________");
        for(int i = 0; i<5; i++)
            System.out.println(grupa[i].toString());



        System.out.println(grupa[3].getNazwisko()+" żyje już - Lat: "+grupa[3].ileLat()+" Miesięcy: "+grupa[3].ileMies()+" Dni: "+grupa[3].ileDni());
    }
    }


