package pl.edu.uwm.wmii.damianszefler.laboratorium09.Zadanie1;
import pl.imiajd.szefler.*;

import java.time.LocalDate;
import java.util.Arrays;

class TestStudent {

    public static void main(String[] args)
    {
        Student[] grupa = new Student[5];

        grupa[0] = new Student("Skiba", LocalDate.of(1996, 12, 12),3.7);
        grupa[1] = new Student("Tabasko", LocalDate.of(1997, 5, 7),6.0);
        grupa[2] = new Student("Skiba", LocalDate.of(1996, 12, 12),4.9);
        grupa[3] = new Student("Stachek", LocalDate.of(1997, 5, 7),3.68);
        grupa[4] = new Student("Szefler", LocalDate.of(1996, 1, 5),3.55);
        for(int i = 0; i<5; i++)
            System.out.println(grupa[i].toString());

        Arrays.sort(grupa, Student::compareTo);
        System.out.println("______________________________________________");
        for(int i = 0; i<5; i++)
            System.out.println(grupa[i].toString());

    }
}


