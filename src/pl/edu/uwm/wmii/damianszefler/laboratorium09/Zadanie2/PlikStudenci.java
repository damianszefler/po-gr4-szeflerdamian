package pl.edu.uwm.wmii.damianszefler.laboratorium09.Zadanie2;

import pl.imiajd.szefler.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;

class PlikStudenci {

    public static void main(String[] args) throws IOException{

        Path pathObj =  Paths.get("src/pl/edu/uwm/wmii/damianszefler/laboratorium09/Zadanie2/dane");
        String path = pathObj.toAbsolutePath().toString();

        File file = new File(path);
        Scanner in = new Scanner(file);

        long lineCount = Files.lines(pathObj).count();

        Student[] grupa = new Student[(int)lineCount];
        String zdanie;

        int i = 0;
         do{
                zdanie = in.nextLine();
                String ostab[] = zdanie.split(" ");
                grupa[i] = new Student(ostab[0], LocalDate.of(Integer.valueOf(ostab[1]), Integer.valueOf(ostab[2]), Integer.valueOf(ostab[3])),Double.valueOf(ostab[4]));
                i++;
        }while(i<(int)lineCount);

        for(i = 0; i<(int)lineCount; i++)
            System.out.println(grupa[i].toString());

        Arrays.sort(grupa, Student::compareTo);
        System.out.println("_________________________________________________________________________");

        for(i = 0; i<(int)lineCount; i++)
            System.out.println(grupa[i].toString());

        }

}
