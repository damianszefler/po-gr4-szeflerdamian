package pl.imiajd.szefler;

import java.awt.Rectangle;

public class BetterRectangle extends Rectangle{
        public BetterRectangle(int width, int height){
            super(width, height);
        }

        public double getPerimeter() {
            return (this.getHeight() + this.getWidth()) * 2;
        }
        public double getArea() {
            return this.getHeight() * this.getWidth();
        }
}
