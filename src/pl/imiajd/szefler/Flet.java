package pl.imiajd.szefler;
import java.time.LocalDate;

public class Flet extends Instrument {
    public Flet(String producent, LocalDate rokProdukcji){
        super(producent,rokProdukcji);
        this.dzwiek();
    }

    @Override
    public String dzwiek()
    {
        return "Fju fju fju";
    }

}
