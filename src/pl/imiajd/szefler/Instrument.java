package pl.imiajd.szefler;

import java.time.LocalDate;

public abstract class Instrument {
    Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji= rokProdukcji;
    }

    private String producent;
    private LocalDate rokProdukcji;

    public String getProducent() {
        return producent;
    }
    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }
    public abstract String dzwiek();

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    @Override
    public String toString() {
        return "["+producent+"]\t\t\t["+rokProdukcji+"]";
    }

}
