package pl.imiajd.szefler;
import java.time.LocalDate;


public class Osoba implements Comparable<Osoba>, Cloneable
{
    public Osoba(String nazwisko, LocalDate data)
    {
        this.nazwisko = nazwisko;
        this.data_ur= data;
    }

    private String nazwisko;
    private LocalDate data_ur;

    public String getNazwisko() { return nazwisko; }
    public int ileLat(){
        return -(this.data_ur.getYear()-LocalDate.now().getYear());
    }
    public int ileMies(){
        return this.data_ur.getMonthValue()-LocalDate.now().getMonthValue();
    }
    public int ileDni(){
        return -(this.data_ur.getDayOfMonth()-LocalDate.now().getDayOfMonth());
    }
    @Override
    public String toString(){
        return "Osoba: \t|\t["+this.nazwisko +"] \t\t\t ["+this.data_ur+"]";
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    @Override
    public int compareTo(Osoba obj) {
        if((this.nazwisko.compareTo(obj.nazwisko)==0))
        {
            if((this.data_ur.compareTo(obj.data_ur)==0))
            { return 0; }
            else if((this.data_ur.compareTo(obj.data_ur)>0))
            { return 1; }
            else
            { return -1; }
        }
        else if((this.nazwisko.compareTo(obj.nazwisko)>0))
        { return 1; }
        else { return -1; }
    }


}