package pl.imiajd.szefler;

import java.time.LocalDate;

public class Student  extends Osoba {
    public Student(String nazwisko, LocalDate data, double srednia) {
        super(nazwisko, data);
        this.srednia = srednia;
    }
    private double srednia;
    public String toString(){return super.toString()+"\t\t\t"+"["+this.srednia+"]";}

    public int compareTo(Student obj) {
        if(super.compareTo(obj)==0)
        {
                if(this.srednia==obj.srednia)
                {
                    return 0;
                }
                else if(this.srednia>obj.srednia)
                { return -1; }
                else { return 1; }
        }
        else if(super.compareTo(obj)>0)
                { return 1; }
        else
                { return -1; }


    }
}
